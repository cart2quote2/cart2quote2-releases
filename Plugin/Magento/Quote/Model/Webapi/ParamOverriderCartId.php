<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Plugin\Magento\Quote\Model\Webapi;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartManagementInterface;

/**
 * Class Quote
 *
 */
class ParamOverriderCartId extends \Magento\Quote\Model\Webapi\ParamOverriderCartId
{
    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var CartManagementInterface
     */
    private $cartManagement;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Cart2Quote\Quotation\Model\Session
     */
    private $quoteSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;

    /**
     * ParamOverriderCartId constructor
     *
     * @param \Magento\Authorization\Model\UserContextInterface $userContext
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagement
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Cart2Quote\Quotation\Model\Session $quoteSession
     * @param \Magento\Checkout\Model\Session $session
     */
    public function __construct(
        UserContextInterface $userContext,
        CartManagementInterface $cartManagement,
        \Magento\Framework\App\RequestInterface $request,
        \Cart2Quote\Quotation\Model\Session $quoteSession,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->quoteSession = $quoteSession;
        $this->request = $request;
        $this->userContext = $userContext;
        $this->cartManagement = $cartManagement;
        $this->session = $session;
        parent::__construct($userContext, $cartManagement);
    }

    /**
     * If the origin URL has quotation then the mine from the REST API
     * - will be the quotation cart id instead of the Magento cart id
     *
     * @param \Magento\Quote\Model\Webapi\ParamOverriderCartId $subject
     * @return int|null
     */
    public function aroundGetOverriddenValue($subject)
    {
        try {
            if ($this->userContext->getUserType() === UserContextInterface::USER_TYPE_CUSTOMER) {
                $referer = $this->request->getHeader('Referer');
                if (str_contains($referer, 'quotation')) {
                    return $this->quoteSession->getQuoteId();
                } else {
                    /** @var \Magento\Quote\Api\Data\CartInterface */
                    $cart = $this->session->getQuote();
                    if ($cart) {
                        return $cart->getId();
                    }
                }
            }
        } catch (NoSuchEntityException $e) {
            //do nothing and just return null
            throw new NoSuchEntityException(__('Current customer does not have an active cart.'));
        }

        return null;
    }
}
