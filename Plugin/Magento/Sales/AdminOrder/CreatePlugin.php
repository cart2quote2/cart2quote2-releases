<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Quotation\Plugin\Magento\Sales\AdminOrder;

use Magento\Sales\Model\AdminOrder\Create;

class CreatePlugin
{

    /**
     * Makes sure product comment doesn't get overwritten by updating order items
     *
     * @param Create $subject
     * @param array $items
     * @return array
     */
    public function beforeUpdateQuoteItems(Create $subject, $items)
    {
        foreach ($items as $itemId => $info) {
            $item = $subject->getQuote()->getItemById($itemId);
            if (!empty($item['description'])) {
                $info['description'] = $item['description'];
            }
        }
        return [$items];
    }
}
