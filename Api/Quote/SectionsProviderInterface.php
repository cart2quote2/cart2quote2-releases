<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Api\Quote;

/**
 * Interface SectionsProviderInterface
 *
 */
interface SectionsProviderInterface
{
    /**
     * Get sections
     *
     * @param int $quoteId
     * @return \Cart2Quote\Quotation\Api\Data\Quote\SectionInterface[]
     */
    public function getSections($quoteId);
}
