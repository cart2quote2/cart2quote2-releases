<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Api;

/**
 * Interface QuoteCartItemRepositoryInterface
 */
interface QuoteCartItemRepositoryInterface
{
    /**
     * Get list
     *
     * @param int $customerId The customer ID.
     * @return \Magento\Quote\Api\Data\CartItemInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     */
    public function getList($customerId);

    /**
     * Delete item by id
     *
     * @param int $customerId The customer ID.
     * @param int $itemId
     * @return bool
     */
    public function deleteById($customerId, $itemId);

    /**
     * Save cart item
     *
     * @param int $customerId The customer ID.
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return \Magento\Quote\Api\Data\CartItemInterface Item.
     */
    public function save($customerId, \Magento\Quote\Api\Data\CartItemInterface $cartItem);

    /**
     * Edit quote item
     *
     * @param int $customerId The customer ID.
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return \Magento\Quote\Api\Data\CartItemInterface Item.
     */
    public function editQuoteItem($customerId, \Magento\Quote\Api\Data\CartItemInterface $cartItem);
}
