<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Api\Data;

/**
 * ExtensionInterface class for @see \Cart2Quote\Quotation\Api\Data\QuoteStatusHistoryInterface
 */
interface QuoteStatusHistoryExtensionInterface extends \Magento\Sales\Api\Data\OrderStatusHistoryExtensionInterface
{
}
