<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Api\Data;

use Magento\Quote\Api\Data\CartInterface;

/**
 * Quote interface
 *
 */
interface QuoteCartInterface extends CartInterface, QuoteInterface
{
    /**
     * Returns the cart shipping address.
     *
     * @return \Magento\Quote\Api\Data\AddressInterface|null Cart shipping address. Otherwise, null.
     */
    public function getShippingAddress();

    /**
     * Sets the cart shipping address.
     *
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @return $this
     */
    public function setShippingAddress(\Magento\Quote\Api\Data\AddressInterface $shippingAddress = null);
}
