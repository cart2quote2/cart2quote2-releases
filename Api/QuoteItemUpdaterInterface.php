<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Api;

interface QuoteItemUpdaterInterface
{
    /**
     * Update quote item by quote ID and item ID.
     *
     * @param int $quoteId
     * @param int $itemId
     * @param array $itemData
     * @return bool
     */
    public function updateQuoteItem($quoteId, $itemId, array $itemData);
}
