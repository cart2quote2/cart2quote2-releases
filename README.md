# Cart2Quote Quotation Encoded

## LICENSE

CART2QUOTE CONFIDENTIAL

[2009] - [2025] Cart2Quote B.V.
All Rights Reserved.
https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)

NOTICE OF LICENSE

All information contained herein is, and remains
the property of Cart2Quote B.V. and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Cart2Quote B.V.
and its suppliers and may be covered by European and Foreign Patents,
patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Cart2Quote B.V.


## SUMMARY

The purpose of the Cart2Quote module is to enrich Magento with 
a complete quotation manager. With Cart2Quote users can create 
quotes in the Magento front-end or backend and manage the complete 
quotation process and at the end convert it to an order.


## REQUIREMENTS

PHP: 		    7.2 / 7.3 / 7.4 / 8.1 / 8.2

MAGENTO:        2.3.3+ / 2.4.0~2.4.7

## INSTALLATION

### OPENSOURCE:

Please follow the installation manual:
https://cart2quote.mesadigital.co.uk/post/cart2quote-installation-manual

### ENCODED (packagist):

1.      php bin magento deploy:mode:set developer

2.      composer require cart2quote/module-quotation-encoded

3.      php bin/magento setup:upgrade

4.      php bin/magento deploy:mode:set production

## CONTACT

Install and Quick Start manual:
https://cart2quote.mesadigital.co.uk/post/cart2quote-installation-manual

User manual:
https://cart2quote.mesadigital.co.uk/post/cart2quote-quotation-user-manual

Request a support ticket:
https://cart2quote.mesadigital.co.uk/submit-request

Request an update or upgrade:
https://cart2quote.mesadigital.co.uk/post/quotation-module-update-manual
