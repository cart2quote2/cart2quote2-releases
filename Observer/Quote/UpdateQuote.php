<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Observer\Quote;

/**
 * Class UpdateQuote
 *
 * @package Cart2Quote\Quotation\Observer\UpdateQuote
 */
class UpdateQuote implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Cart2Quote\Quotation\Model\Session
     */
    private $session;

    /**
     * Item constructor.
     *
     * @param \Cart2Quote\Quotation\Model\Session $session
     */
    public function __construct(\Cart2Quote\Quotation\Model\Session $session)
    {
        $this->session = $session;
    }

    /**
     * Execute
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quotationFieldData = $this->session->getData($this->session::QUOTATION_FIELD_DATA);
        $quotationFieldData = is_array($quotationFieldData) ? $quotationFieldData : [];
        $this->session->addFieldData($quotationFieldData);
        return $this;
    }
}
