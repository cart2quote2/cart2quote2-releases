<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper;

class LockQuote extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @const quote form settings config path
     */
    const LOCK_QUOTE = 'quotation_backend/backend_options/quote_statuses';

    /**
     * Get quote statuses config array
     *
     * @return array
     */
    public function getQuoteStatusesConfigArray()
    {
        $config = $this->scopeConfig->getValue(
            self::LOCK_QUOTE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($config !== null) {
            return json_decode($config, true);
        }
    }
}
