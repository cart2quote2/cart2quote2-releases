<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper;

/**
 * Class LockQuote
 */
class QuoteItems extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @const quote form settings config path
     */
    const QUOTE_ITEMS = 'quotation_backend/backend_options/item_configuration';

    /**
     * Get quote items config array
     *
     * @return array
     */
    public function getQuoteItemsConfigArray()
    {
        $config = $this->scopeConfig->getValue(
            self::QUOTE_ITEMS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($config !== null) {
            return json_decode($config, true);
        }
    }
}
