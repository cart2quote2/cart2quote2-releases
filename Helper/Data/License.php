<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper\Data;

class License implements \Cart2Quote\Quotation\Helper\Data\LicenseInterface
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    private $backendUrl;

    /**
     * License constructor.
     *
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     */
    public function __construct(\Magento\Backend\Model\UrlInterface $backendUrl)
    {
        $this->backendUrl = $backendUrl;
    }

    /**
     * Get expiry date
     *
     * @return string|null
     */
    public function getExpiryDate()
    {
        return null;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return \Laminas\Uri\UriFactory::factory($this->backendUrl->getBaseUrl())->getHost();
    }

    /**
     * Get licensen type
     *
     * @return string
     */
    public function getLicenseType()
    {
        return 'one_off';
    }

    /**
     * Get license state
     *
     * @return string
     */
    public function getLicenseState()
    {
        return 'active';
    }

    /**
     * Check is active state
     *
     * @return bool
     */
    public function isActiveState()
    {
        return true;
    }

    /**
     * Check is inactive state
     *
     * @return bool
     */
    public function isInactiveState()
    {
        return false;
    }

    /**
     * Check is pending state
     *
     * @return bool
     */
    public function isPendingState()
    {
        return false;
    }

    /**
     * Check is unreachable state
     *
     * @return bool
     */
    public function isUnreachable()
    {
        return false;
    }

    /**
     * Check is unreachable state (pass through)
     *
     * @return bool
     */
    public function isUnreachableState()
    {
        return $this->isUnreachable();
    }

    /**
     * Get proposal amount
     *
     * @return int
     */
    public function getProposalAmount()
    {
        return 0;
    }

    /**
     * Get edition
     *
     * @return string
     */
    public function getEdition()
    {
        return 'opensource';
    }

    /**
     * Get order id
     *
     * @return string|null
     */
    public function getOrderId()
    {
        return null;
    }

    /**
     * Get simplified license state
     *
     * @return int
     */
    public function getSimplifiedLicenseState()
    {
        return License::OPENSOURCE;
    }

    /**
     * Check is allowed for edition
     *
     * @param string $edition
     * @return bool
     */
    public function isAllowedForEdition($edition = 'opensource')
    {
        return true;
    }

    /**
     * Is trial
     *
     * @return bool
     */
    public function isTrial()
    {
        return false;
    }

    /**
     * Is mp version
     *
     * @return bool
     */
    public function isMpVersion()
    {
        if ($this->getDomain() == 'magento.local') {
            return true;
        }

        if (str_contains($this->getDomain(), 'magentosite.cloud')) {
            return true;
        }

        return false;
    }
}
