<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper\Reports;

use Cart2Quote\Quotation\Model\ResourceModel\Cron\Collection as CronResourceCollection;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;

class PotentialIssues extends \Magento\Framework\App\Helper\AbstractHelper
{
    const DEFAULT_MAX_SESSION_SIZE_IN_ADMIN = 256000;

    const SUFFICIENT_MAX_SESSION_SIZE_IN_ADMIN = 512000;

    const MAX_SESSION_SIZE_IN_ADMIN_XML_PATH = 'system/security/max_session_size_admin';

    /**
     * @var CronResourceCollection
     */
    protected $cronResourceModelCollection;

    /**
     * @var CacheTypeListInterface
     */
    protected $cacheTypeInterface;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * PotentialIssues constructor
     *
     * @param CronResourceCollection $cronResourceModelCollection
     * @param CacheTypeListInterface $cacheTypeInterface
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        CronResourceCollection                          $cronResourceModelCollection,
        CacheTypeListInterface                          $cacheTypeInterface,
        \Magento\Framework\App\Helper\Context           $context,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->cronResourceModelCollection = $cronResourceModelCollection;
        $this->cacheTypeInterface = $cacheTypeInterface;
        $this->productMetadata = $productMetadata;
        parent::__construct($context);
    }

    /**
     * Get the potential issue
     *
     * @return array
     */
    public function getPotentialIssues()
    {
        $allIssues = [];
        $cacheValidationCheck = $this->cacheTypeInterface->getInvalidated();

        if ($cacheValidationCheck) {
            foreach ($cacheValidationCheck as $potentialIssue) {
                $cacheType = $potentialIssue->getCacheType();
                if ($cacheType == 'Page Cache' || $cacheType == 'Configuration') {
                    array_push($allIssues, $cacheType);
                }
            }
        }

        $cronTasks = $this->cronResourceModelCollection->count();

        if (!$cronTasks) {
            array_push($allIssues, 'Cron Configuration');
        }

        // Session size unlimited when set to '0'
        $maxSessionSize = $this->isMaxSessionSizeInAdminSufficient();
        if (!$maxSessionSize && $maxSessionSize !== 0) {
            array_push($allIssues, 'Magento 2.4.3 Issue');
        }

        return $allIssues;
    }

    /**
     * Get all potential issues and issue data
     *
     * @return array
     */
    public function getAllPotentialIssuesData()
    {
        return $this->addIssueData($this->getPotentialIssues());
    }

    /**
     * Get the cron data
     *
     * @return bool
     */
    public function getCronData()
    {
        $cart2QuoteCronEnabled = $this->cronResourceModelCollection->getItemsByColumnValue(
            'job_code',
            'cart2quote_send_autoproposal_emails'
        );

        if ($cart2QuoteCronEnabled) {
            return true;
        }

        return false;
    }

    /**
     * Add data to each potential issue case
     *
     * @param array $potentialIssues
     * @return array
     */
    public function addIssueData(array $potentialIssues)
    {
        $potentialIssuesData = [];
        foreach ($potentialIssues as $potentialIssue) {
            switch ($potentialIssue) {
                case 'Page Cache':
                    $potentialIssue = [
                        'reference' => __('Page Cache'),
                        'description' => __(
                            'Your cache type has been invalidated.
                             This could result in configuration/output inconsistencies.'
                        ),
                        'solution' => __('Flush cache')
                    ];

                    array_push($potentialIssuesData, $potentialIssue);
                    break;

                case 'Cron Configuration':
                    $potentialIssue = [
                        'reference' => __('Cron Configuration'),
                        'description' => __(
                            'Server cron is not active.
                            Cart2Quote delayed emails and autoproposal sending delays will not work.'
                        ),
                        'solution' => __('Enable Cron')
                    ];

                    array_push($potentialIssuesData, $potentialIssue);
                    break;

                case 'Configuration':
                    $potentialIssue = [
                        'reference' => __('Configuration Cache'),
                        'description' => __(
                            'Your cache type has been invalidated.
                             This could result in configuration/output inconsistencies.'
                        ),
                        'solution' => __('Flush cache')
                    ];

                    array_push($potentialIssuesData, $potentialIssue);
                    break;

                case 'Magento 2.4.3 Issue':
                    $potentialIssue = [
                        'reference' => __('Magento 2.4.3 Issue'),
                        'description' => __('Products cannot be added to the quote in admin.'),
                        'solution' => __(
                            'Your "Max Session Size in Admin" is set to %1 but Magento recommends %2.
                            Go to Stores->Configuration->Advanced->System->Security to change it.',
                            $this->getMaxSessionSizeInAdmin(),
                            self::SUFFICIENT_MAX_SESSION_SIZE_IN_ADMIN
                        )
                    ];

                    array_push($potentialIssuesData, $potentialIssue);
                    break;
            }
        }

        return $potentialIssuesData;
    }

    /**
     * Function to check if max session size in admin is sufficient
     *
     * @return bool
     */
    private function isMaxSessionSizeInAdminSufficient()
    {
        if ($this->productMetadata->getVersion() >= '2.4.3') {
            return $this->getMaxSessionSizeInAdmin() >= self::SUFFICIENT_MAX_SESSION_SIZE_IN_ADMIN;
        }
        return true;
    }

    /**
     * Function to get max session size in admin configuration value
     *
     * @return int
     */
    private function getMaxSessionSizeInAdmin()
    {
        $maxSessionSizeInAdmin = $this->scopeConfig->getValue(
            self::MAX_SESSION_SIZE_IN_ADMIN_XML_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $maxSessionSizeInAdmin =
            $maxSessionSizeInAdmin === null ? self::DEFAULT_MAX_SESSION_SIZE_IN_ADMIN : $maxSessionSizeInAdmin;
        return $maxSessionSizeInAdmin;
    }
}
