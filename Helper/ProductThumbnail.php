<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper;

use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Item;

class ProductThumbnail extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Path to configuration setting Show Thumb Nail in Request Mail
     */
    const XML_PATH_SHOW_THUMB_NAIL_REQUEST = "quotation_email/quote_request/product_thumbnail";

    /**
     * Path to configuration setting Show Thumb Nail in Proposal Mail
     */
    const XML_PATH_SHOW_THUMB_NAIL_PROPOSAL = "quotation_email/quote_proposal/product_thumbnail";

    /**
     * Path to configuration setting Show Thumb Nail in Proposal Mail
     */
    const XML_PATH_SHOW_THUMB_NAIL_PDF = "quotation_advanced/pdf/product_thumbnail";

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    protected $productHelper;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Url interface
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlModel;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $configurableProductType;

    /**
     * @var \Magento\GroupedProduct\Model\Product\Type\Grouped
     */
    protected $groupedProductType;

    /**
     * @var \Magento\Bundle\Model\Product\Type
     */
    protected $bundleProductType;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * ProductThumbnail constructor
     *
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Backend\Block\Template\Context $blockContext
     */
    public function __construct(
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Block\Template\Context $blockContext,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductType,
        \Magento\GroupedProduct\Model\Product\Type\Grouped $groupedProductType,
        \Magento\Bundle\Model\Product\Type $bundleProductType,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->productHelper = $productHelper;
        $this->urlModel = $context->getUrlBuilder();
        $this->storeManager = $blockContext->getStoreManager();
        $this->assetRepo = $blockContext->getAssetRepository();
        $this->configurableProductType = $configurableProductType;
        $this->groupedProductType = $groupedProductType;
        $this->bundleProductType = $bundleProductType;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * Is the Product Thumbnail enabled for the Request Email
     *
     * @return bool
     */
    public function showProductThumbnailRequest()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_THUMB_NAIL_REQUEST,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Is the Product Thumbnail enabled for the Proposal Email
     *
     * @return bool
     */
    public function showProductThumbnailProposal()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_THUMB_NAIL_PROPOSAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Is the Product Thumbnail enabled for the PDF
     *
     * @return bool
     */
    public function showProductThumbnailPdf()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_THUMB_NAIL_PDF,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the Product Thumbnail for Request Email
     *
     * @param Product|\Magento\Framework\DataObject $product
     * @return string
     */
    public function getProductThumbnail($product)
    {
        $url = $this->productHelper->getThumbnailUrl($product);
        if (!empty($url)) {
            return $url;
        }

        $attribute = $product->getResource()->getAttribute('thumbnail');
        if (!$product->getThumbnail()) {
            $url = $this->assetRepo->getUrl('Magento_Catalog::images/product/placeholder/thumbnail.jpg');
        } elseif ($attribute) {
            $url = $attribute->getFrontend()->getUrl($product);
        }

        return $url;
    }

    /**
     * Get the product url from the quote item
     * (This also works for multi store urls)
     *
     * @param Item $item
     * @return string
     */
    public function getProductUrl($item)
    {
        if ($item->getRedirectUrl()) {
            return $item->getRedirectUrl();
        }

        /** @var Product $product */
        $product = $item->getProduct();
        $option = $item->getOptionByCode('product_type');
        if ($option) {
            $product = $option->getProduct();
        }

        $product = $this->getParentProduct($product);
        if (!$product) {
            return null;
        }

        return $product->getUrlModel()->getUrl($product);
    }

    /**
     * Get parent product if it exists, otherwise return null.
     *
     * @param Product $product
     * @return \Magento\Catalog\Api\Data\ProductInterface|null
     */
    private function getParentProduct(Product $product)
    {
        if ($parentIds = $this->configurableProductType->getParentIdsByChild($product->getId())) {
            if (isset($parentIds[0])) {
                return $this->productRepository->getById((int)$parentIds[0]);
            }
            return null;
        }

        if ($parentIds = $this->groupedProductType->getParentIdsByChild($product->getId())) {
            if (isset($parentIds[0])) {
                return $this->productRepository->getById((int)$parentIds[0]);
            }
            return null;
        }

        if ($parentIds = $this->bundleProductType->getParentIdsByChild($product->getId())) {
            if (isset($parentIds[0])) {
                return $this->productRepository->getById((int)$parentIds[0]);
            }
            return null;
        }

        return $product;
    }
}
