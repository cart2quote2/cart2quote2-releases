<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Helper;

class FileUpload extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Path to allowed_file_extensions in system.xml
     */
    const XML_PATH_ALLOWED_FILE_EXTENSION = 'quotation_advanced/quotation_options/allowed_file_extensions';

    /**
     * Get allowed file extensions
     *
     * @return array|null
     */
    public function getAllowedFileExtensions()
    {
        $allowedExtensions = $this->scopeConfig->getValue(
            self::XML_PATH_ALLOWED_FILE_EXTENSION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        //failsafe if no allowed file extensions are set
        $allowedExtensionsTrimmed = '*';

        if ($allowedExtensions) {
            $allowedExtensionsTrimmed = preg_replace('/\s*,\s*/', ',', $allowedExtensions);
        }

        return preg_split('/,/', $allowedExtensionsTrimmed);
    }
}
