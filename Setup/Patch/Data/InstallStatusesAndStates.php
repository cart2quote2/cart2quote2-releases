<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Data;

class InstallStatusesAndStates implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var \Magento\Eav\Api\Data\AttributeSetInterfaceFactory
     */
    private $attributeSetFactory;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    private $entityType;

    /**
     * @var array[]
     */
    private $attributeSetData = [
        'attribute_set_name' => 'Install statuses and states',
    ];

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @var \Magento\Eav\Api\AttributeManagementInterface
     */
    private $attributeManagement;

    /**
     * @var \Cart2Quote\Quotation\Setup\QuoteSetup
     */
    private $quoteSetup;

    /**
     * @param \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup
     * @param \Magento\Eav\Api\AttributeManagementInterface $attributeManagement
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
     * @param \Magento\Eav\Api\Data\AttributeSetInterfaceFactory $attributeSetFactory
     * @param \Magento\Eav\Model\Entity\Type $type
     */
    public function __construct(
        \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup,
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository,
        \Magento\Eav\Api\Data\AttributeSetInterfaceFactory $attributeSetFactory,
        \Magento\Eav\Model\Entity\Type $type
    ) {
        $this->quoteSetup = $quoteSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->entityType = $type->loadByCode(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE);
        $this->moduleDataSetup = $moduleDataSetup;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeManagement = $attributeManagement;
    }

    /**
     * Function to add attribute set
     *
     * @return $this|InstallStatusesAndStates
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $catalogSetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);

        $groupName = 'Product Details';
        $entityTypeId = $catalogSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $catalogSetup->getDefaultAttributeSetId($entityTypeId);

        if (!$catalogSetup->getAttributesNumberInGroup($entityTypeId, $attributeSetId, 'Quotable')) {
            $catalogSetup->removeAttributeGroup($entityTypeId, $attributeSetId, 'Quotable');
        }

        /**
         * Install quote statuses from config
         */
        $data = [];

        $statuses = [
            'open' => __('Open'),
            'new' => __('New'),
            'processing' => __('In Process'),
            'change_request' => __('Change Request'),
            'quote_available' => __('Quote Available'),
            'holded' => __('On Hold'),
            'waiting_supplier' => __('Waiting for supplier'),
            'canceled' => __('Canceled'),
            'out_of_stock' => __('Out of Stock'),
            'proposal_expired' => __('Proposal Expired'),
            'pending' => __('Pending'),
            'proposal_sent' => __('Proposal sent'),
            'ordered' => __('Ordered'),
            'accepted' => __('Accepted'),
            'closed' => __('Closed'),
        ];
        foreach ($statuses as $code => $info) {
            $data[] = ['status' => $code, 'label' => $info];
        }

        if (count($this->quoteSetup->getConnection()->fetchAll(
            $this->quoteSetup->getConnection()->select()->from(
                $this->quoteSetup->getTable('quotation_quote_status'),
                '*'
            )
        ))) {
            $this->quoteSetup->getConnection()->insertOnDuplicate(
                $this->quoteSetup->getTable('quotation_quote_status'),
                $data,
                ['status', 'label']
            );
        }

        $quoteStatusTable = $this->quoteSetup->getTable('quotation_quote_status');
        $connection = $this->quoteSetup->getConnection();

        $select = $connection->select()
            ->from($quoteStatusTable);

        $queryResult = $connection->fetchAll($select);

        if (empty($queryResult)) {
            $this->quoteSetup->getConnection()->insertOnDuplicate(
                $this->quoteSetup->getTable('quotation_quote_status'),
                $data,
                ['status', 'label']
            );
        }

        /**
         * Install quote states from config
         */
        $data = [];
        $states = [
            'open' => [
                'label' => __('Open'),
                'statuses' => [
                    'open' => ['default' => '0'],
                    'new' => ['default' => '1'],
                    'change_request' => ['default' => '0'],
                    'processing' => ['default' => '0'],
                ],
                'visible_on_front' => true,
            ],
            'holded' => [
                'label' => __('On Hold'),
                'statuses' => [
                    'holded' => ['default' => '1'],
                    'waiting_supplier' => ['default' => '0'],
                ],
                'visible_on_front' => true,
            ],
            'canceled' => [
                'label' => __('Canceled'),
                'statuses' => [
                    'canceled' => ['default' => '1'],
                    'out_of_stock' => ['default' => '0'],
                    'proposal_expired' => ['default' => '0'],
                ],
                'visible_on_front' => true,
            ],
            'pending' => [
                'label' => __('Pending'),
                'statuses' => [
                    'pending' => ['default' => '1'],
                    'proposal_sent' => ['default' => '0'],
                    'quote_available' => ['default' => '0'],
                ],
                'visible_on_front' => true,
            ],
            'completed' => [
                'label' => __('Completed'),
                'statuses' => [
                    'ordered' => ['default' => '0'],
                    'accepted' => ['default' => '1'],
                    'closed' => ['default' => '0'],
                ],
                'visible_on_front' => true,
            ],
        ];

        foreach ($states as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status => $statusInfo) {
                    $isDefault = 0;
                    if (is_array($statusInfo) && isset($statusInfo['default'])) {
                        $isDefault = $statusInfo['default'];
                    }

                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => $isDefault,
                    ];
                }
            }
        }

        $this->quoteSetup->getConnection()->insertOnDuplicate(
            $this->quoteSetup->getTable('quotation_quote_status_state'),
            $data,
            ['status', 'state', 'is_default']
        );

        /** Update visibility for states */
        foreach ($states as $index => $state) {
            $this->quoteSetup->getConnection()->update(
                $this->quoteSetup->getTable('quotation_quote_status_state'),
                ['visible_on_front' => $state['visible_on_front']],
                ['state = ?' => $index]
            );
        }
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
