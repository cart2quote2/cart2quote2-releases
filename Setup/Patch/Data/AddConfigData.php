<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AddConfigData implements DataPatchInterface
{
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @var \Cart2Quote\Quotation\Helper\Data\Metadata
     */
    public $metadata;

    /**
     * Add config data constructor
     *
     * @param WriterInterface $configWriter
     * @param \Cart2Quote\Quotation\Helper\Data\Metadata $metadata
     */
    public function __construct(
        WriterInterface $configWriter,
        \Cart2Quote\Quotation\Helper\Data\Metadata $metadata
    ) {
        $this->configWriter = $configWriter;
        $this->metadata = $metadata;
    }

    /**
     * Applies necessary configurations and setups for the module
     *
     * @return void
     */
    public function apply()
    {
        $value = (version_compare($this->metadata->getMagentoVersion(), '2.4.7', '>=')) ? 1 : 0;
        $this->configWriter->save(
            'quotation_advanced/magento_version/247orgreater',
            $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
    }

    /**
     * Function to revert attribute set
     *
     * @return void
     */
    public function revert()
    {
        $this->configWriter->delete(
            'quotation_advanced/magento_version/247orgreater',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
