<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Data;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Mail\MimePartInterfaceFactory;

class AddCustomProduct implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productModel;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $moduleReader;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\OptionFactory
     */
    protected $productOptionFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $filesystemIo;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Framework\Filesystem\DriverInterface
     */
    private $fileSystem;

    /**
     * @var \Magento\Framework\Mail\MimePartInterfaceFactory
     */
    private $mimePartInterfaceFactory;

    /**
     * AddCustomProduct constructor.
     *
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     * @param \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Filesystem\Io\File $filesystemIo
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Filesystem\Io\File $filesystemIo,
        \Magento\Framework\App\State $state
    ) {
        $this->productModel = $productModel;
        $this->objectManager = $objectManager;
        $this->moduleReader = $moduleReader;
        $this->productFactory = $productFactory;
        $this->productOptionFactory = $productOptionFactory;
        $this->productRepository = $productRepository;
        $this->filesystemIo = $filesystemIo;
        $this->state = $state;
        $this->fileSystem = $this->objectManager->get(File::class);
        $this->mimePartInterfaceFactory = $objectManager->get(MimePartInterfaceFactory::class);
    }

    /**
     * Applies necessary configurations and setups for the module
     *
     * @return AddCustomProduct|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\ValidatorException
     */
    public function apply()
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        }

        if (!$this->productModel->getIdBySku('custom-product')) {
            $options = [];

            $options[] = [
                'title' => 'name',
                'type' => 'field',
                'is_require' => true,
                'sort_order' => 1,
                'price' => 0,
                'price_type' => 'fixed',
                'max_characters' => 50,
            ];

            $options[] = [
                'title' => 'sku',
                'type' => 'field',
                'is_require' => true,
                'sort_order' => 2,
                'price' => 0,
                'price_type' => 'fixed',
                'max_characters' => 50,
            ];

            $options[] = [
                'title' => 'price',
                'type' => 'field',
                'is_require' => true,
                'sort_order' => 3,
                'price' => 0,
                'price_type' => 'fixed',
                'max_characters' => 50,
            ];

            $viewDir = $this->moduleReader->getModuleDir(
                \Magento\Framework\Module\Dir::MODULE_VIEW_DIR,
                'Cart2Quote_Quotation'
            );

            $imagePath = $viewDir . '/adminhtml/web/images/custom_product.png';
            if ($this->fileSystem->isFile($imagePath)) {
                /* @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                $mediaDirectory = $this->objectManager->get(\Magento\Framework\Filesystem::class)
                    ->getDirectoryRead(DirectoryList::MEDIA);
                $copyToPath = $mediaDirectory->getAbsolutePath() . 'quotation/custom_product.png';
                $this->filesystemIo->cp($imagePath, $copyToPath);
            }

            /* @var \Magento\Catalog\Model\Product $customProduct */
            $customProduct = $this->productFactory->create();
            $customProduct->setName('Custom Product')
                ->setSku('custom-product')
                ->setPrice(0)
                ->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                ->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE)
                ->setAttributeSetId($customProduct->getDefaultAttributeSetId())
                ->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                ->setStockData(
                    [
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 0
                    ]
                );

            if (isset($copyToPath) && $this->fileSystem->isFile($copyToPath)) {
                $customProduct->addImageToMediaGallery(
                    $copyToPath,
                    [
                        'image',
                        'small_image',
                        'thumbnail'
                    ],
                    false,
                    false
                );
            }

            $customProduct->save();

            foreach ($options as $option) {
                $customOption = $this->productOptionFactory->create(['data' => $option]);
                $customOption->setProductSku($customProduct->getSku());
                $customOptions[] = $customOption;
            }

            if (isset($customOptions)) {
                $product = $this->productRepository->getById($customProduct->getId());
                $product->setCanSaveCustomOptions(true)
                    ->setOptions($customOptions)
                    ->setHasOptions(true)
                    ->save();
            }
        }
    }

    /**
     * Function to revert attribute set
     *
     * @return void
     */
    public function revert()
    {
        if ($this->productModel->getIdBySku('custom-product')) {
            $this->productRepository->deleteById('custom-product');
        }
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
