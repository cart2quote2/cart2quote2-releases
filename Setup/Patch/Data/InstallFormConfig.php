<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Data;

/**
 * Install default billing & shipping address config
 */
class InstallFormConfig implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    private $configInterface;

    /**
     * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
     */
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
    ) {
        $this->configInterface = $configInterface;
    }

    /**
     * Function to add default Billing & Shipping Config
     *
     * @return $this|InstallFormConfig
     */
    public function apply()
    {
        $shippingAddressData = $this->getShippingAddressDefaultConfig();
        $serializedShippingData = json_encode($shippingAddressData);
        $this->configInterface->saveConfig(
            'quotation_advanced/quote_form/shipping_address_grid',
            $serializedShippingData,
            'default',
            0
        );

        $billingAddressData = $this->getBillingAddressDefaultConfig();
        $serializedBillingData = json_encode($billingAddressData);
        $this->configInterface->saveConfig(
            'quotation_advanced/quote_form/billing_address_grid',
            $serializedBillingData,
            'default',
            0
        );
    }

    /**
     * @return array[]
     */
    public function getShippingAddressDefaultConfig(){
        $shippingAddressData = [
            [
                "label" => "Prefix",
                "name" => "prefix",
                "required" => false,
                "additionalCss" => "",
                "enabled" => false,
                "locked" => false,
                "sortOrder" => 5
            ],
            [
                "label" => "First Name",
                "name" => "firstname",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 10
            ],
            [
                "label" => "Middlename",
                "name" => "middlename",
                "required" => false,
                "additionalCss" => "",
                "enabled" => false,
                "locked" => false,
                "sortOrder" => 15
            ],
            [
                "label" => "Last Name",
                "name" => "lastname",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 20
            ],
            [
                "label" => "Suffix",
                "name" => "suffix",
                "required" => false,
                "additionalCss" => "",
                "enabled" => false,
                "locked" => false,
                "sortOrder" => 25
            ],
            [
                "label" => "Company",
                "name" => "company",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => false,
                "sortOrder" => 30
            ],
            [
                "label" => "Street Address",
                "name" => "street",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 40
            ],
            [
                "label" => "City",
                "name" => "city",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 50
            ],
            [
                "label" => "State/Province",
                "name" => "region_id",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 60
            ],
            [
                "label" => "Zip/Postal Code",
                "name" => "postcode",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 70
            ],
            [
                "label" => "Country",
                "name" => "country_id",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => true,
                "sortOrder" => 80
            ],
            [
                "label" => "Phone Number",
                "name" => "telephone",
                "required" => true,
                "additionalCss" => "",
                "enabled" => true,
                "locked" => false,
                "sortOrder" => 90
            ],
            [
                "label" => "Tax/VAT Number",
                "name" => "vat_id",
                "required" => false,
                "additionalCss" => "",
                "enabled" => false,
                "locked" => false,
                "sortOrder" => 100
            ],
            [
                "label" => "Fax Number",
                "name" => "fax",
                "required" => false,
                "additionalCss" => "",
                "enabled" => false,
                "locked" => false,
                "sortOrder" => 110
            ]
        ];

        return $shippingAddressData;
    }

    /**
     * @return array[]
     */
    public function getBillingAddressDefaultConfig(){
        $billingAddressData = [
            [
                'label' => 'Prefix',
                'name' => 'prefix',
                'additionalCss' => '',
                'locked' => false,
                'sortOrder' => 5,
            ],
            [
                'label' => 'First Name',
                'name' => 'firstname',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 10,
            ],
            [
                'label' => 'Middlename',
                'name' => 'middlename',
                'required' => false,
                'additionalCss' => '',
                'enabled' => false,
                'locked' => false,
                'sortOrder' => 15,
            ],
            [
                'label' => 'Last Name',
                'name' => 'lastname',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 20,
            ],
            [
                'label' => 'Suffix',
                'name' => 'suffix',
                'required' => false,
                'additionalCss' => '',
                'enabled' => false,
                'locked' => false,
                'sortOrder' => 25,
            ],
            [
                'label' => 'Company',
                'name' => 'company',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => false,
                'sortOrder' => 30,
            ],
            [
                'label' => 'Street Address',
                'name' => 'street',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 40,
            ],
            [
                'label' => 'City',
                'name' => 'city',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 50,
            ],
            [
                'label' => 'State/Province',
                'name' => 'region_id',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 60,
            ],
            [
                'label' => 'Zip/Postal Code',
                'name' => 'postcode',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 70,
            ],
            [
                'label' => 'Country',
                'name' => 'country_id',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => true,
                'sortOrder' => 80,
            ],
            [
                'label' => 'Phone Number',
                'name' => 'telephone',
                'required' => true,
                'additionalCss' => '',
                'enabled' => true,
                'locked' => false,
                'sortOrder' => 90,
            ],
            [
                'label' => 'Tax/VAT Number',
                'name' => 'vat_id',
                'required' => false,
                'additionalCss' => '',
                'enabled' => false,
                'locked' => false,
                'sortOrder' => 100,
            ],
            [
                'label' => 'Fax Number',
                'name' => 'fax',
                'required' => false,
                'additionalCss' => '',
                'enabled' => false,
                'locked' => false,
                'sortOrder' => 110,
            ],
        ];

        return $billingAddressData;
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
