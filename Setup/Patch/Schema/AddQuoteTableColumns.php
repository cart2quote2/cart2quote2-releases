<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Schema;

/**
 * Class AddQuoteTableColumns
 * @package Cart2Quote\Quotation\Setup\Patch\Schema
 */
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;

class AddQuoteTableColumns implements SchemaPatchInterface, PatchRevertableInterface
{
    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeSetInterfaceFactory
     */
    private $attributeSetFactory;

    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    private $entityType;

    /**
     * @var array[]
     */
    private $attributeSetData = [
        'attribute_set_name' => 'Configurable Wallpaper',
    ];

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @var \Magento\Eav\Api\AttributeManagementInterface
     */
    private $attributeManagement;

    /**
     * @var \Cart2Quote\Quotation\Setup\QuoteSetup
     */
    private $quoteSetup;

    /**
     * Add quote table constructor
     *
     * @param \Magento\Eav\Api\AttributeManagementInterface $attributeManagement
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
     * @param \Magento\Eav\Model\Entity\Type $type
     * @param \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup
     */
    public function __construct(
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository,
        \Magento\Eav\Model\Entity\Type $type,
        \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup
    ) {
        $this->attributeSetRepository = $attributeSetRepository;
        $this->entityType = $type->loadByCode(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE);
        $this->moduleDataSetup = $moduleDataSetup;
        $this->attributeManagement = $attributeManagement;
        $this->quoteSetup = $quoteSetup;
    }

    /**
     * Applies necessary configurations and setups for the module
     *
     * @return $this|AddQuoteTableColumns
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        if ($this->quoteSetup->getConnection()->isTableExists($this->quoteSetup->getTable('quote'))) {
            $this->quoteSetup->getConnection()->addColumn(
                $this->quoteSetup->getTable('quote'),
                'is_quotation_quote',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'unsigned' => true,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'Linked Quotation Quote'
                ]
            );

            $this->quoteSetup->getConnection()->addColumn(
                $this->quoteSetup->getTable('quote'),
                'linked_quotation_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'unsigned' => true,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Linked Quotation Quote'
                ]
            );
        }

        $quotationQuoteTable = $this->quoteSetup->getTable('quotation_quote');
        $this->quoteSetup->getConnection()->addForeignKey(
            $this->quoteSetup->getConnection()->getForeignKeyName(
                $this->quoteSetup->getTable('quote'),
                \Magento\Quote\Model\Quote::KEY_ENTITY_ID,
                $quotationQuoteTable,
                'quote_id'
            ),
            $this->quoteSetup->getTable('quote'),
            'linked_quotation_id',
            $quotationQuoteTable,
            'quote_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_NO_ACTION
        );

        $this->moduleDataSetup->getConnection()->endSetup();

        return $this;
    }

    /**
     * Function to revert attribute set
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        if ($this->quoteSetup->getConnection()->isTableExists($this->quoteSetup->getTable('quote'))) {
            $this->quoteSetup->getConnection()->dropColumn($this->quoteSetup->getTable('quote'), 'is_quotation_quote');

            $this->quoteSetup->getConnection()->dropColumn($this->quoteSetup->getTable('quote'), 'linked_quotation_id');
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
