<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Setup\Patch\Schema;

/**
 * Class AddQuotationSequence
 * @package Cart2Quote\Quotation\Setup\Patch\Schema
 */
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;

class AddQuotationSequence implements SchemaPatchInterface, PatchRevertableInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Cart2Quote\Quotation\Setup\QuoteSetup
     */
    protected $quoteSetup;

    /**
     * @var \Cart2Quote\Quotation\Setup\SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var \Magento\SalesSequence\Model\Builder
     */
    protected $sequenceBuilder;

    /**
     * @var \Cart2Quote\Quotation\Model\SalesSequence\Config
     */
    protected $sequenceConfig;

    /**
     * @var \Magento\Catalog\Setup\CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @var \Cart2Quote\Quotation\Model\SalesSequence\EntityPool
     */
    private $entityPool;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Cart2Quote\Quotation\Model\SalesSequence\EntityPool $entityPool
     * @param \Cart2Quote\Quotation\Setup\SalesSetupFactory $salesSetupFactory
     * @param \Magento\SalesSequence\Model\Builder $sequenceBuilder
     * @param \Cart2Quote\Quotation\Model\SalesSequence\Config $sequenceConfig
     * @param \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
     * @param \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Cart2Quote\Quotation\Model\SalesSequence\EntityPool $entityPool,
        \Cart2Quote\Quotation\Setup\SalesSetupFactory $salesSetupFactory,
        \Magento\SalesSequence\Model\Builder $sequenceBuilder,
        \Cart2Quote\Quotation\Model\SalesSequence\Config $sequenceConfig,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Cart2Quote\Quotation\Setup\QuoteSetup $quoteSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->entityPool = $entityPool;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->sequenceBuilder = $sequenceBuilder;
        $this->sequenceConfig = $sequenceConfig;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->quoteSetup = $quoteSetup;
    }

    /**
     * Applies necessary configurations and setups for the module
     *
     * @return $this|AddQuotationSequence
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Sales\Setup\SalesSetup $salesSetup */
        $salesSetup = $this->salesSetupFactory->create();

        /**
         * Install eav entity types to the eav/entity_type table
         */
        $salesSetup->installEntities();

        $defaultStoreIds = [0, 1];
        foreach ($defaultStoreIds as $storeId) {
            foreach ($salesSetup->getDefaultEntities() as $entityType => $entity) {
                if ($entityType == \Cart2Quote\Quotation\Model\SalesSequence\EntityPool::QUOTATION_ENTITY) {
                    $this->sequenceBuilder->setPrefix($this->sequenceConfig->get('prefix'))
                        ->setSuffix($this->sequenceConfig->get('suffix'))
                        ->setStartValue($this->sequenceConfig->get('startValue'))
                        ->setStoreId($storeId)
                        ->setStep($this->sequenceConfig->get('step'))
                        ->setWarningValue($this->sequenceConfig->get('warningValue'))
                        ->setMaxValue($this->sequenceConfig->get('maxValue'))
                        ->setEntityType($entityType)
                        ->create();
                }
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();

        return $this;
    }

    /**
     * Reverts necessary configurations and setups for the module
     *
     * @return void
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        ////        // TODO: Write revert script, inspiration:
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Get the dependencies
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get the aliases
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}
