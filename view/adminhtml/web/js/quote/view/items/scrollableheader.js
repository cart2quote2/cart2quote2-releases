/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

require([ "jquery" ], function($){

    $(window).scroll(function () {
        if($(window).scrollTop() > $('.actions').offset().top && !($('.actions').hasClass('c2q-scrolling'))) {
            $('.actions').addClass('c2q-scrolling');
        } else if ($(window).scrollTop() < 356) {
            $('.actions').removeClass('c2q-scrolling');
        }
    });
});
