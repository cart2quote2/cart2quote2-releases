<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Controller\Adminhtml\Quote;

class View extends \Cart2Quote\Quotation\Controller\Adminhtml\Quote
{
    /**
     * View quote detail
     *
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $quote = $this->_initQuote();
        $this->_initSession();
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($quote) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                if ($quoteItem->getProduct()['status'] == '2') {
                    $this->messageManager->addWarningMessage(__('Warning %1 is disabled!', $quoteItem->getName()));
                }
            }
            if ($quote->getState() == \Cart2Quote\Quotation\Model\Quote\Status::STATE_OPEN &&
                $quote->getStatus() == \Cart2Quote\Quotation\Model\Quote\Status::STATUS_NEW
            ) {
                $quote->setState(\Cart2Quote\Quotation\Model\Quote\Status::STATE_OPEN)
                    ->setStatus(\Cart2Quote\Quotation\Model\Quote\Status::STATUS_OPEN)->save();
            }
            if (!$quote->getCustomerIsGuest()) {
                try {
                    $this->customerRepositoryInterface->getById($quote->getCustomerId());
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('Customer no longer exists.'));
                }
            }
            try {
                $resultPage = $this->_initAction();
                $resultPage->getConfig()->getTitle()->prepend(__('Quotes'));
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage(__('Exception occurred during quote load'));
                $resultRedirect->setPath('quotation/quote/index');

                return $resultRedirect;
            }

            $resultPage->getConfig()->getTitle()->prepend(sprintf('#%s', $quote->getIncrementId()));

            return $resultPage;
        }
        $resultRedirect->setPath('quotation/*/');

        return $resultRedirect;
    }

    /**
     * ACL check
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cart2Quote_Quotation::actions_view');
    }
}
