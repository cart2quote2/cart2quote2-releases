<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Controller\Adminhtml\Quote\Ajax;

class SubtotalProposalApply extends AbstractAction
{
    /**
     * Handle an AJAX request for updating subtotal proposal in a quote.
     *
     * @return \Magento\Backend\Model\View\Result\Page|null
     */
    public function execute()
    {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noroute');
        }

        $amount = (float)$this->getRequest()->getParam('amount');
        $isPercentage = $this->getRequest()->getParam('isPercentage') === 'true';

        try {
            $this->_initQuote();
            $quote = $this->getCurrentQuote()->setSubtotalProposal($amount, $isPercentage);
            $quote->saveQuote();

            $this->getMessageManager()->addSuccessMessage(__('Subtotal proposal is applied'));

            return $this->getResponse()->representJson(
                $this->json->serialize(['success' => true])
            );
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->getMessageManager()->addExceptionMessage($e);

            return $this->getResponse()->representJson(
                $this->json->serialize(['error' => true])
            );
        }
    }
}
