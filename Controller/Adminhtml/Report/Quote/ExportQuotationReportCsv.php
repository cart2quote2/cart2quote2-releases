<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Controller\Adminhtml\Report\Quote;

use Magento\Framework\App\Filesystem\DirectoryList;

class ExportQuotationReportCsv extends \Cart2Quote\Quotation\Controller\Adminhtml\Report\Quote\BaseReport
{
    /**
     * Return file instance created by file factory
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $fileName = 'quotationreport.csv';
        $grid = $this->createGrid();

        return $this->_fileFactory->create($fileName, $grid->getCsvFile(), DirectoryList::VAR_DIR);
    }
}
