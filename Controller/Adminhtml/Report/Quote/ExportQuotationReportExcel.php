<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Controller\Adminhtml\Report\Quote;

use Magento\Framework\App\Filesystem\DirectoryList;

class ExportQuotationReportExcel extends \Cart2Quote\Quotation\Controller\Adminhtml\Report\Quote\BaseReport
{
    /**
     * Execute the function and create a file instance using the file factory
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $fileName = 'quotationreport.xml';
        $grid = $this->createGrid();

        return $this->_fileFactory->create($fileName, $grid->getExcelFile($fileName), DirectoryList::VAR_DIR);
    }
}
