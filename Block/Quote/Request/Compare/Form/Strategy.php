<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Quote\Request\Compare\Form;

class Strategy
{
    //Empty class for compatibility with older Magento 2.2 versions
}
