<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Quote\Request\Button;

class QuickQuote extends Button implements \Cart2Quote\Quotation\Model\Strategy\StrategyInterface
{
    /**
     * @var string
     */
    protected $_template = 'Cart2Quote_Quotation::product/view/quote/request/quickquote/button.phtml';

    /**
     * @var bool
     */
    protected $modalIsRendered = false;

    /**
     * Get Modal to HTML
     *
     * @param string $modalBlockName
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getModalHtml($modalBlockName)
    {
        return $this->modalIsRendered ?: $this->getLayout()->getBlock($modalBlockName)->toHtml();
    }
}
