<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Quote;

use Magento\Framework\View\Element\Template;

class Files extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\File
     */
    protected $fileModel;

    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    protected $urlEncoder;

    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    protected $quotationHelper;

    /**
     * @var \Cart2Quote\Quotation\Model\QuoteRepository
     */
    protected $quotationRepository;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $ioFile;

    /**
     * Files constructor.
     *
     * @param \Cart2Quote\Quotation\Model\Quote\File $fileModel
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Cart2Quote\Quotation\Helper\Data $quotationHelper
     * @param \Cart2Quote\Quotation\Model\QuoteRepository $quotationRepository
     * @param Template\Context $context
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Model\Quote\File $fileModel,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Cart2Quote\Quotation\Helper\Data $quotationHelper,
        \Cart2Quote\Quotation\Model\QuoteRepository $quotationRepository,
        Template\Context $context,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->fileModel = $fileModel;
        $this->urlEncoder = $urlEncoder;
        $this->quotationHelper = $quotationHelper;
        $this->quotationRepository = $quotationRepository;
        $this->ioFile = $ioFile;
    }

    /**
     * Get files
     *
     * @return array|null
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getFiles()
    {
        $quoteId = $this->getRequest()->getParam('quote_id');
        $quotationOrigId = $this->quotationRepository->get($quoteId)->getOrigQuotationId();

        if (!$quotationOrigId == 0 && $this->quotationHelper->quoteChangesVisibility()) {
            $quoteId = $quotationOrigId;
        }

        return $this->fileModel->getFiles($quoteId, $this->fileModel::CUSTOMER_FOLDER);
    }

    /**
     * Get download url
     *
     * @param string $file
     * @return string
     */
    public function getDownloadUrl($file)
    {
        $file = $this->urlEncoder->encode($file);
        return $this->getUrl('quotation/fileupload/download', ['file' => $file]);
    }

    /**
     * Trim file name
     *
     * @param string $file
     * @return bool|string
     */
    public function trimFileName($file)
    {
        return $this->ioFile->getPathInfo($file)['basename'];
    }
}
