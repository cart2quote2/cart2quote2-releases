<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Proposal;

class Reject extends \Cart2Quote\Quotation\Block\Quote\View
{
    /**
     * Get is reject proposal enabled
     *
     * @return bool
     */
    public function getIsRejectProposalEnabled()
    {
        return $this->quotationHelper->isRejectProposalEnabled();
    }

    /**
     * Get reject url
     *
     * @return string
     */
    public function getRejectUrl()
    {
        return $this->getUrl('quotation/proposal/view', ['quote_id' => $this->getQuote()->getEntityId()]);
    }
}
