<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Tab;

use Magento\Backend\Block\Widget\Tab\TabInterface;

class File extends \Cart2Quote\Quotation\Block\Adminhtml\Quote\AbstractQuote implements TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'quote/view/details/quote/uploadedfiles.phtml';

    /**
     * @var \Cart2Quote\Quotation\Model\Quote\File
     */
    protected $fileModel;

    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    protected $urlEncoder;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $ioFile;

    /**
     * File constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Cart2Quote\Quotation\Model\Quote\File $fileModel
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Cart2Quote\Quotation\Model\Quote\File $fileModel,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->fileModel = $fileModel;
        $this->urlEncoder = $urlEncoder;
        $this->ioFile = $ioFile;
    }

    /**
     * Retrieve quote model instance
     *
     * @return \Cart2Quote\Quotation\Model\Quote
     */
    public function getQuote()
    {
        return $this->_coreRegistry->registry('current_quote');
    }

    /**
     * Get Table Title
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabTitle()
    {
        return __('Quote Files');
    }

    /**
     * Get Tab label
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabLabel()
    {
        return __('Uploaded Files');
    }

    /**
     * Check if tab can be shown
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Check if tab is hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Get the uploaded files
     *
     * @return array
     */
    public function getUploadedFiles()
    {
        return $this->fileModel->getFileDataFromQuotation();
    }

    /**
     * Get the download url
     *
     * @param string $file
     * @return string
     */
    public function getDownloadUrl($file)
    {
        $file = $this->urlEncoder->encode($file);
        return $this->getUrl('quotation/file/download', ['file' => $file]);
    }

    /**
     * Get the delete url
     *
     * @param string $file
     * @return string
     */
    public function getDeleteUrl($file)
    {
        $file = $this->urlEncoder->encode($file);
        $quoteId = $this->getRequest()->getParam('quote_id');

        return $this->getUrl('quotation/file/remove', ['file' => $file, 'quote_id' => $quoteId]);
    }

    /**
     * Get checkbox id
     *
     * @param string $file
     * @return string
     */
    public function getCheckboxId($file)
    {
        return $this->trimFileName($file);
    }

    /**
     * Trim file name
     *
     * @param string $file
     * @return bool|string
     */
    public function trimFileName($file)
    {
        return $this->ioFile->getPathInfo($file)['basename'];
    }

    /**
     * Is checked or not
     *
     * @param string $file
     * @param string $location
     * @return bool
     */
    public function isChecked($file, $location)
    {
        $quoteId = $this->getRequest()->getParam('quote_id');
        $file = $this->ioFile->getPathInfo($file)['basename'];

        return $this->fileModel->visible($file, $quoteId, $location);
    }

    /**
     * Get action url
     *
     * @return string
     */
    public function getUrlAction()
    {
        return $this->getUrl('quotation/file/save');
    }
}
