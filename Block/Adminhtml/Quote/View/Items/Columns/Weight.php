<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items\Columns;

class Weight extends \Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items\DefaultRenderer
{
    /**
     * @var \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Weight
     */
    public $weight;

    /**
     * Weight constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\QuoteItems $quoteItemsHelper
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product\OptionFactory $optionFactory
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Item $emptyQuoteItem
     * @param \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
     * @param \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer
     * @param \Cart2Quote\Quotation\Helper\CostPrice $costPriceHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\CatalogRule\Model\ResourceModel\Rule $catalogRule
     * @param \Cart2Quote\Quotation\Helper\StockCheck $stockHelper
     * @param \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Weight $weight
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\QuoteItems $quoteItemsHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Cart2Quote\Quotation\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Item $emptyQuoteItem,
        \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper,
        \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer,
        \Cart2Quote\Quotation\Helper\CostPrice $costPriceHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\CatalogRule\Model\ResourceModel\Rule $catalogRule,
        \Cart2Quote\Quotation\Helper\StockCheck $stockHelper,
        \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Weight $weight,
        array $data = []
    ) {
        $this->weight = $weight;
        parent::__construct(
            $quoteItemsHelper,
            $imageHelper,
            $productRepositoryInterface,
            $context,
            $stockRegistry,
            $stockConfiguration,
            $registry,
            $optionFactory,
            $quote,
            $emptyQuoteItem,
            $quotationTaxHelper,
            $itemPriceRenderer,
            $costPriceHelper,
            $dateTime,
            $catalogRule,
            $stockHelper,
            $data
        );
    }

    /**
     * Get product weight
     *
     * @param float $weight
     * @return string|null
     */
    public function getFormattedWeight($weight)
    {
        $weight = $weight ? (float) $weight : 0;
        $this->weight->setValue($weight);

        return floatval($this->weight->getEscapedValue()) . $this->getWeightUnit();
    }

    /**
     * Get total weight of all products
     *
     * @param array $quote
     * @return string|null
     */
    public function getTotalWeight($quote)
    {
        $items = $quote->getAllVisibleItems();
        $weight = 0;

        if (!empty($items)) {
            foreach ($items as $item) {
                if (!empty($item->getWeight())) {
                    $weight += ($item->getWeight() * $item->getQty());
                }
            }
        }

        return $this->getFormattedWeight($weight);
    }

    /**
     * Get weight unit
     *
     * @return string
     */
    public function getWeightUnit()
    {
        return $this->_scopeConfig->getValue(
            'general/locale/weight_unit',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
