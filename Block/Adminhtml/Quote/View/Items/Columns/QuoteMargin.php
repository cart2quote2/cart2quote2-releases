<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items\Columns;

use Magento\Quote\Model\Quote\Item;

class QuoteMargin extends \Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items\DefaultRenderer
{
    const TOTAL = 'total';
    const INDIVIDUAL = 'individual';
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;

    /**
     * @var \Cart2Quote\Quotation\Helper\MarginCalculation
     */
    protected $marginCalculationHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     */
    public $productRepositoryInterface;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Magento\CatalogRule\Model\ResourceModel\Rule
     */
    protected $catalogRule;

    /**
     * @var \Cart2Quote\Quotation\Helper\StockCheck
     */
    public $stockHelper;

    /**
     * QuoteMargin constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\QuoteItems $quoteItemsHelper
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     * @param \Cart2Quote\Quotation\Helper\MarginCalculation $marginCalculationHelper
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product\OptionFactory $optionFactory
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Item $emptyQuoteItem
     * @param \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
     * @param \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer
     * @param \Cart2Quote\Quotation\Helper\CostPrice $costPriceHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\CatalogRule\Model\ResourceModel\Rule $catalogRule
     * @param \Cart2Quote\Quotation\Helper\StockCheck $stockHelper
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\QuoteItems $quoteItemsHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Cart2Quote\Quotation\Helper\MarginCalculation $marginCalculationHelper,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Cart2Quote\Quotation\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Item $emptyQuoteItem,
        \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper,
        \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer,
        \Cart2Quote\Quotation\Helper\CostPrice $costPriceHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\CatalogRule\Model\ResourceModel\Rule $catalogRule,
        \Cart2Quote\Quotation\Helper\StockCheck $stockHelper,
        array $data = []
    ) {
        parent::__construct(
            $quoteItemsHelper,
            $imageHelper,
            $productRepositoryInterface,
            $context,
            $stockRegistry,
            $stockConfiguration,
            $registry,
            $optionFactory,
            $quote,
            $emptyQuoteItem,
            $quotationTaxHelper,
            $itemPriceRenderer,
            $costPriceHelper,
            $dateTime,
            $catalogRule,
            $stockHelper,
            $data
        );

        $this->marginCalculationHelper = $marginCalculationHelper;
    }

    /**
     * Get total margin
     *
     * @param float $quoteTotal
     * @param float $costTotal
     * @return float
     */
    public function getTotalMargin($quoteTotal, $costTotal)
    {
        return $this->marginCalculationHelper->calculatePercentage($quoteTotal, $costTotal);
    }

    /**
     * Get total margin value
     *
     * @param string $quoteTotal
     * @param float $costTotal
     * @return float
     */
    public function getTotalMarginValue($quoteTotal, $costTotal)
    {
        $quoteCurrencyCode = $this->getQuote()->getQuoteCurrencyCode();

        return $this->marginCalculationHelper->calculateValue((float)$quoteTotal, $costTotal, $quoteCurrencyCode);
    }

    /**
     * Calculate the profit Margin based on
     * - the item's cost price and the quoted price.
     *
     * @param Item $item
     * @return string
     */
    public function getMargin(\Magento\Quote\Model\Quote\Item $item)
    {
        $margin = $this->marginCalculationHelper->itemMargin($item);
        if ($margin) {
            return $margin;
        }

        return null;
    }

    /**
     * Get margin value
     *
     * @param Item $item
     * @return string|null
     */
    public function getMarginValue(\Magento\Quote\Model\Quote\Item $item)
    {
        return $this->marginCalculationHelper->itemMarginValue($item) ?? null;
    }
}
