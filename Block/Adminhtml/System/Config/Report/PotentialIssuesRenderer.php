<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\System\Config\Report;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field as FormField;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;

class PotentialIssuesRenderer extends FormField implements RendererInterface
{
    /**
     * @var \Cart2Quote\Quotation\Helper\Reports\PotentialIssues
     */
    protected $potentialIssuesHelper;

    /**
     * PotentialIssuesRenderer constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\Reports\PotentialIssues $potentialIssuesHelper
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\Reports\PotentialIssues $potentialIssuesHelper,
        Context $context,
        array $data = []
    ) {
        $this->potentialIssuesHelper = $potentialIssuesHelper;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve HTML markup for given form element
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $url = $this->getUrl('quotation/report_possibleissues');
        $issueCount = count($this->potentialIssuesHelper->getPotentialIssues());

        if ($issueCount) {
            $buttonText = __('View Possible Issues');
            $element->addCustomAttribute(
                'onClick',
                "window.open('$url', 'about:blank', 'width=934, height=800')"
            );

            $html = '<p>' . __('%1 possible issue(s) have been detected. Click below to view.', $issueCount) . '</p>';
        } else {
            $buttonText = __('No issues');
            $html = '<p>' . __('No possible issues have been detected.') . '</p>';
        }

        $element->setValue($buttonText);
        $class = 'action-default scalable ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only';
        $element->setClass($class);

        $html .= $this->_renderValue($element);

        return $this->_decorateRowHtml($element, $html);
    }
}
