<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\System\Config\Form\Field;

use Cart2Quote\Quotation\Helper\Data\License as License;

class LicenseType extends Template
{
    /**
     * Render the license type
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        if ($this->license->getSimplifiedLicenseState() == License::PENDING_LICENSE ||
            $this->license->getEdition() == 'lite') {
            return '';
        }

        return parent::render($element);
    }

    /**
     * Apply the element config
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     */
    protected function applyElementConfig(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->setValue(__($this->license->getLicenseType()));
        parent::applyElementConfig($element);
    }

    /**
     * After get element html
     *
     * @return string
     */
    protected function afterGetElementHtml()
    {
        $html = parent::afterGetElementHtml();
        $html .= '</div>';

        return $html;
    }

    /**
     * Before get element html
     *
     * @return string
     */
    protected function beforeGetElementHtml()
    {
        $html = parent::beforeGetElementHtml();
        $html .= '<div class="value-container">';

        return $html;
    }
}
