<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Block\Adminhtml\Report\System;

/**
 * Class PotentialIssuess
 *
 */
class PotentialIssues extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Cart2Quote\Quotation\Helper\Reports\PotentialIssues
     */
    protected $potentialIssuesHelper;

    /**
     * PotentialIssuess constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\Reports\PotentialIssues $potentialIssuesHelper
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\Quotation\Helper\Reports\PotentialIssues $potentialIssuesHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->potentialIssuesHelper = $potentialIssuesHelper;
        parent::__construct($context, $data);
    }

    /**
     * Count potential issues
     *
     * @return array
     */
    public function countPotentialIssues()
    {
        return $this->potentialIssuesHelper->getPotentialIssues();
    }

    /**
     * Get potential issues data
     *
     * @return array
     */
    public function getPotentialIssuesData()
    {
        return $this->potentialIssuesHelper->getAllPotentialIssuesData();
    }
}
