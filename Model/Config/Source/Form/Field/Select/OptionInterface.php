<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\Config\Source\Form\Field\Select;

/**
 * Interface OptionInterface
 *
 */
interface OptionInterface
{
    /**
     * Get label
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getLabel();

    /**
     * Get comment
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getComment();
}
