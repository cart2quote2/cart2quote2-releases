<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\Email;

use Magento\Framework\Mail\MimePartInterfaceFactory;

class ZendTree extends ZendAdapter
{
    use \Cart2Quote\Features\Traits\Model\Email\ZendTree {
        attachFileAdapter as private traitAttachFileAdapter;
        getMessageAdapter as private traitGetMessageAdapter;
    }

    /**
     * @var \Magento\Framework\Mail\MimePartInterfaceFactory
     */
    private $mimePartInterfaceFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\Filesystem\DriverInterface
     */
    private $fileSystem;

    /**
     * @var bool
     */
    private $isLaminas;

    /**
     * ZendTree constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
        $this->fileSystem = $this->objectManager->get(\Magento\Framework\Filesystem\Driver\File::class);
        $this->mimePartInterfaceFactory = $objectManager->get(MimePartInterfaceFactory::class);
        $this->isLaminas = class_exists(\Laminas\Mime\Part::class);
    }

    /**
     * Get attach file adapter
     *
     * @param string $file
     * @param string $name
     * @return \Zend\Mime\Part
     */
    public function attachFileAdapter($file, $name)
    {
        return $this->traitAttachFileAdapter($file, $name);
    }

    /**
     * Get message adapter
     *
     * @param array $attachedPart
     * @param string $body
     * @param \Magento\Framework\Mail\Message|\Zend\Mime\Message|null $message
     */
    // @codingStandardsIgnoreStart
    public function getMessageAdapter($attachedPart, $body, $message = null)
    {
        $this->traitGetMessageAdapter($attachedPart, $body, $message);
    }
    // @codingStandardsIgnoreEnd
}
