<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\IsProductSalableForRequestedQtyCondition;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Math\Division as MathDivision;
use Magento\Framework\Phrase;
use Magento\InventoryConfigurationApi\Api\Data\StockItemConfigurationInterface;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventoryReservationsApi\Model\GetReservationsQuantityInterface;
use Magento\InventorySalesApi\Api\Data\ProductSalabilityErrorInterfaceFactory;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterface;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterfaceFactory;
use Magento\InventorySalesApi\Model\GetStockItemDataInterface;
use Magento\InventorySales\Model\IsProductSalableForRequestedQtyCondition\IsCorrectQtyCondition as CorrectQtyCondition;

class IsCorrectQtyCondition extends CorrectQtyCondition
{
    use \Cart2Quote\Features\Traits\Model\IsProductSalableForRequestedQtyCondition\IsCorrectQtyCondition {
        execute as private traitExecute;
        createErrorResult as private traitCreateErrorResult;
        isDecimalQtyCheckFailed as private traitIsDecimalQtyCheckFailed;
        isMinSaleQuantityCheckFailed as private traitIsMinSaleQuantityCheckFailed;
        isMaxSaleQuantityCheckFailed as private traitIsMaxSaleQuantityCheckFailed;
        isQuantityIncrementCheckFailed as private traitIsQuantityIncrementCheckFailed;
    }

    /**
     * @var GetStockItemConfigurationInterface
     */
    private $getStockItemConfiguration;

    /**
     * @var GetReservationsQuantityInterface
     */
    private $getReservationsQuantity;

    /**
     * @var GetStockItemDataInterface
     */
    private $getStockItemData;

    /**
     * @var StockConfigurationInterface
     */
    private $configuration;

    /**
     * @var MathDivision
     */
    private $mathDivision;

    /**
     * @var ProductSalabilityErrorInterfaceFactory
     */
    private $productSalabilityErrorFactory;

    /**
     * @var ProductSalableResultInterfaceFactory
     */
    private $productSalableResultFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * IsCorrectQtyCondition constructor.
     *
     * @param GetStockItemConfigurationInterface $getStockItemConfiguration
     * @param StockConfigurationInterface $configuration
     * @param GetReservationsQuantityInterface $getReservationsQuantity
     * @param GetStockItemDataInterface $getStockItemData
     * @param MathDivision $mathDivision
     * @param ProductSalabilityErrorInterfaceFactory $productSalabilityErrorFactory
     * @param ProductSalableResultInterfaceFactory $productSalableResultFactory
     * @param RequestInterface $request
     */
    public function __construct(
        GetStockItemConfigurationInterface $getStockItemConfiguration,
        StockConfigurationInterface $configuration,
        GetReservationsQuantityInterface $getReservationsQuantity,
        GetStockItemDataInterface $getStockItemData,
        MathDivision $mathDivision,
        ProductSalabilityErrorInterfaceFactory $productSalabilityErrorFactory,
        ProductSalableResultInterfaceFactory $productSalableResultFactory,
        RequestInterface $request
    ) {
        $this->getStockItemConfiguration = $getStockItemConfiguration;
        $this->configuration = $configuration;
        $this->getStockItemData = $getStockItemData;
        $this->getReservationsQuantity = $getReservationsQuantity;
        $this->mathDivision = $mathDivision;
        $this->productSalabilityErrorFactory = $productSalabilityErrorFactory;
        $this->productSalableResultFactory = $productSalableResultFactory;
        $this->request = $request;
    }

    /**
     * @inheritdoc
     */
    public function execute(string $sku, int $stockId, float $requestedQty): ProductSalableResultInterface
    {
        return $this->traitExecute($sku, $stockId, $requestedQty);
    }

    /**
     * Create Error Result Object
     *
     * @param string $code
     * @param Phrase $message
     * @return ProductSalableResultInterface
     */
    private function createErrorResult(string $code, Phrase $message): ProductSalableResultInterface
    {
        return $this->traitCreateErrorResult($code, $message);
    }

    /**
     * Check if decimal quantity is valid
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isDecimalQtyCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        return $this->traitIsDecimalQtyCheckFailed($stockItemConfiguration, $requestedQty);
    }

    /**
     * Check if min sale condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isMinSaleQuantityCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        return $this->traitIsMinSaleQuantityCheckFailed($stockItemConfiguration, $requestedQty);
    }

    /**
     * Check if max sale condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isMaxSaleQuantityCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        return $this->traitIsMaxSaleQuantityCheckFailed($stockItemConfiguration, $requestedQty);
    }

    /**
     * Check if increment quantity condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isQuantityIncrementCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
        return $this->traitIsQuantityIncrementCheckFailed($stockItemConfiguration, $requestedQty);
    }
}
