<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\Strategy;

/**
 * Interface ProviderInterface
 *
 */
interface ProviderInterface
{
    /**
     * Get quote request strategy
     *
     * @return \Cart2Quote\Quotation\Model\Strategy\StrategyInterface
     */
    public function getStrategy();
}
