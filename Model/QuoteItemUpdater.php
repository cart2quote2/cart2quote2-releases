<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model;

use Cart2Quote\Quotation\Api\QuoteItemUpdaterInterface;
use Magento\Quote\Model\Quote\ItemFactory;
use Cart2Quote\Quotation\Model\Quote\Item\Updater;

class QuoteItemUpdater implements QuoteItemUpdaterInterface
{
    use \Cart2Quote\Features\Traits\Model\QuoteItemUpdater {
        updateQuoteItem as private traitUpdateQuoteItem;
    }

    /**
     * @var \Magento\Quote\Model\Quote\ItemFactory
     */
    private $itemFactory;

    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Item\Updater
     */
    private $quoteItemUpdater;

    /**
     * @param ItemFactory $itemFactory
     * @param Updater $quoteItemUpdater
     */
    public function __construct(
        ItemFactory $itemFactory,
        Updater     $quoteItemUpdater
    )
    {
        $this->itemFactory = $itemFactory;
        $this->quoteItemUpdater = $quoteItemUpdater;
    }

    /**
     * Update quote item by quote ID and item ID.
     *
     * @param int $quoteId
     * @param int $itemId
     * @param array $itemData
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateQuoteItem($quoteId, $itemId, array $itemData)
    {
        return $this->traitUpdateQuoteItem($quoteId, $itemId, $itemData);
    }
}
