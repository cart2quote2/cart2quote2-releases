<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\Quote;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;

class File
{
    use \Cart2Quote\Features\Traits\Model\Quote\File {
        uploadFiles as private traitUploadFiles;
        setFilesVisibleToCustomer as private traitSetFilesVisibleToCustomer;
        removeFile as private traitRemoveFile;
        setImageDataToSession as private traitSetImageDataToSession;
        getFileDataFromSession as private traitGetFileDataFromSession;
        saveFileQuotationQuote as private traitSaveFileQuotationQuote;
        getFileDataFromQuotation as private traitGetFileDataFromQuotation;
        fileAction as private traitFileAction;
        addTo as private traitAddTo;
        copyFilesToNewQuote as private traitCopyFilesToNewQuote;
        visible as private traitVisible;
        getFiles as private traitGetFiles;
    }

    const FILE_DOWNLOAD = 'download';
    const FILE_DELETE = 'delete';
    const CUSTOMER_FOLDER = 'customer';
    const EMAIL_FOLDER = 'email';
    const SHOW_CUSTOMER = 'show_customer';
    const SHOW_EMAIL = 'show_email';
    const DONT_EMAIL = 'dont_email';
    const QUOTATION_FOLDER = 'quotation';
    const QUOTATION_ROOT = '';

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var \Cart2Quote\Quotation\Model\Session
     */
    private $quoteSession;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $fileDriver;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $ioFile;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    private $backendSessionQuote;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    private $fileFactory;

    /**
     * @var \Cart2Quote\Quotation\Helper\FileUpload
     */
    private $fileUploadHelper;

    /**
     * @var \Magento\Downloadable\Helper\Download
     */
    protected $downloadHelper;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var \Cart2Quote\Quotation\Helper\QuotationTaxHelper
     */
    protected $quotationTaxHelper;

    /**
     * File constructor.
     *
     * @param \Magento\Downloadable\Helper\Download $downloadHelper
     * @param \Cart2Quote\Quotation\Helper\FileUpload $fileUploadHelper
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Backend\Model\Session\Quote $backendSessionQuote
     * @param \Magento\Framework\Filesystem\Io\File $io
     * @param \Cart2Quote\Quotation\Model\Session $quoteSession
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
     */
    public function __construct(
        \Magento\Downloadable\Helper\Download $downloadHelper,
        \Cart2Quote\Quotation\Helper\FileUpload $fileUploadHelper,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Backend\Model\Session\Quote $backendSessionQuote,
        \Magento\Framework\Filesystem\Io\File $io,
        \Cart2Quote\Quotation\Model\Session $quoteSession,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\App\Request\Http $request,
        \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
    ) {
        $this->downloadHelper = $downloadHelper;
        $this->fileUploadHelper = $fileUploadHelper;
        $this->fileFactory = $fileFactory;
        $this->backendSessionQuote = $backendSessionQuote;
        $this->ioFile = $io;
        $this->fileDriver = $fileDriver;
        $this->quoteSession = $quoteSession;
        $this->filesystem = $filesystem;
        $this->uploaderFactory = $uploaderFactory;
        $this->request = $request;
        $this->quotationTaxHelper = $quotationTaxHelper;
    }

    /**
     * Upload the files
     *
     * @param int $fileAmount
     * @param bool $backend
     * @param int $quoteId
     * @return array
     * @throws \Exception
     */
    public function uploadFiles($fileAmount, $backend = false, $quoteId = null)
    {
        return $this->traitUploadFiles($fileAmount, $backend, $quoteId);
    }

    /**
     * Make files which were uploaded by the customer automatically visible to the customer on the frontend
     *
     * @param int $quoteId
     * @throws FileSystemException
     */
    public function setFilesVisibleToCustomer($quoteId)
    {
        $this->traitSetFilesVisibleToCustomer($quoteId);
    }

    /**
     * Remove the file
     *
     * @param string $fileName
     * @return void
     * @throws FileSystemException
     */
    public function removeFile($fileName)
    {
        $this->traitRemoveFile($fileName);
    }

    /**
     * Set the image data to the session
     *
     * @param array $imageData
     */
    public function setImageDataToSession($imageData)
    {
        $this->traitSetImageDataToSession($imageData);
    }

    /**
     * Get the file data from the session
     *
     * @return array|null
     */
    public function getFileDataFromSession()
    {
        return $this->traitGetFileDataFromSession();
    }

    /**
     * Save the files of quotation quote
     *
     * @param int $quoteId
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function saveFileQuotationQuote($quoteId)
    {
        $this->traitSaveFileQuotationQuote($quoteId);
    }

    /**
     * Get the file data from quotation
     *
     * @return array
     */
    public function getFileDataFromQuotation()
    {
        return $this->traitGetFileDataFromQuotation();
    }

    /**
     * Get the file action
     *
     * @param string $fileName
     * @param string $action
     * @return \Magento\Framework\App\ResponseInterface|string
     * @throws \Exception
     */
    public function fileAction($fileName, $action)
    {
        return $this->traitFileAction($fileName, $action);
    }

    /**
     * Add files to frontend and mail
     *
     * @param array $files
     * @param string $quoteId
     * @param string $location
     * @return array|bool
     * @throws FileSystemException
     */
    public function addTo($files, $quoteId, $location)
    {
        return $this->traitAddTo($files, $quoteId, $location);
    }

    /**
     * Copy the files to a new quote
     *
     * @param array $files
     * @param int $quoteId
     * @param string $location
     * @throws FileSystemException
     */
    public function copyFilesToNewQuote($files, $quoteId, $location)
    {
        $this->traitCopyFilesToNewQuote($files, $quoteId, $location);
    }

    /**
     * Set the visibility of files
     *
     * @param string $file
     * @param string $quoteId
     * @param string $location
     * @return bool
     */
    public function visible($file, $quoteId, $location)
    {
        return $this->traitVisible($file, $quoteId, $location);
    }

    /**
     * Get the files
     *
     * @param string $quoteId
     * @param string $location
     * @return array|null
     */
    public function getFiles($quoteId, $location)
    {
        return $this->traitGetFiles($quoteId, $location);
    }
}
