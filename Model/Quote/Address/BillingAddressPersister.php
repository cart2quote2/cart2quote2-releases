<?php

namespace Cart2Quote\Quotation\Model\Quote\Address;

class BillingAddressPersister extends \Magento\Quote\Model\Quote\Address\BillingAddressPersister
{
    use \Cart2Quote\Features\Traits\Model\Quote\Address\BillingAddressPersister {
        save as private traitSave;
    }

    /**
     * @var \Magento\Quote\Model\QuoteAddressValidator
     */
    private $addressValidator;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * BillingAddressPersister constructor.
     *
     * @param \Magento\Quote\Model\QuoteAddressValidator $addressValidator
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Psr\Log\LoggerInterface $_logger
     */
    public function __construct(
        \Magento\Quote\Model\QuoteAddressValidator $addressValidator,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $_logger
    ) {
        parent::__construct($addressValidator, $addressRepository);
        $this->addressValidator = $addressValidator;
        $this->addressRepository = $addressRepository;
        $this->request = $request;
        $this->_logger = $_logger;
    }

    /**
     * Save address for billing.
     *
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @param \Magento\Quote\Api\Data\AddressInterface $address
     * @param bool $useForShipping
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function save(
        \Magento\Quote\Api\Data\CartInterface $quote,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    ) {
        $this->traitSave($quote, $address, $useForShipping);
    }
}
