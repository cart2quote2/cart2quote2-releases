<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\Quote\Address\Total;

class QuoteAdjustment extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    use \Cart2Quote\Features\Traits\Model\Quote\Address\Total\QuoteAdjustment {
        fetch as private traitFetch;
    }

    /**
     * @var \Magento\Weee\Helper\Data
     */
    protected $weeeHelper;

    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    protected $cart2QuoteHelper;

    /**
     * QuoteAdjustment constructor
     *
     * @param \Magento\Weee\Helper\Data $weeeHelper
     * @param \Cart2Quote\Quotation\Helper\Data $cart2QuoteHelper
     */
    public function __construct(
        \Magento\Weee\Helper\Data $weeeHelper,
        \Cart2Quote\Quotation\Helper\Data $cart2QuoteHelper
    ) {
        $this->weeeHelper = $weeeHelper;
        $this->cart2QuoteHelper = $cart2QuoteHelper;
    }

    /**
     * Assign quote adjustment amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        return $this->traitFetch($quote, $total);
    }
}
