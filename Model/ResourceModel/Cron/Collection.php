<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model\ResourceModel\Cron;

/**
 * Collection resource model
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    use \Cart2Quote\Features\Traits\Model\ResourceModel\Cron\Collection {
        _construct as private _traitConstruct;
    }

    /**
     * @var string
     */
    protected $_idFieldName = 'schedule_id';

    /**
     * Collection model initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_traitConstruct();
    }
}
