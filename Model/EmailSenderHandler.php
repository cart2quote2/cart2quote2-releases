<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\Quotation\Model;

/**
 * Quotation emails sending observer.
 * Performs handling of cron jobs related to sending emails to customers
 * after creation/modification of Order, Invoice, Shipment or Creditmemo.
 */
class EmailSenderHandler
{
    use \Cart2Quote\Features\Traits\Model\EmailSenderHandler {
        sendEmails as private traitSendEmails;
        getEmailSender as private traitGetEmailSender;
        registerAsyncQuotation as private traitRegisterAsyncQuotation;
    }

    /**
     * Email sender model.
     *
     * @var \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender
     */
    protected $emailSender;

    /**
     * Entity resource model.
     *
     * @var \Magento\Sales\Model\ResourceModel\EntityAbstract
     */
    protected $entityResource;

    /**
     * Entity collection model.
     *
     * @var \Magento\Sales\Model\ResourceModel\Collection\AbstractCollection
     */
    protected $entityCollection;

    /**
     * Global configuration storage.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $globalConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * EmailSenderHandler constructor
     *
     * @param \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender $emailSender
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote $entityResource
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Collection $entityCollection
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender $emailSender,
        \Cart2Quote\Quotation\Model\ResourceModel\Quote $entityResource,
        \Cart2Quote\Quotation\Model\ResourceModel\Quote\Collection $entityCollection,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        \Magento\Framework\Registry $registry
    ) {
        $this->emailSender = $emailSender;
        $this->entityResource = $entityResource;
        $this->entityCollection = $entityCollection;
        $this->globalConfig = $globalConfig;
        $this->coreRegistry = $registry;
    }

    /**
     * Handles asynchronous email sending
     *
     * @param array $ignoreStatus list of statuses that shouln't be handled by this module
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendEmails($ignoreStatus = [])
    {
        $this->traitSendEmails($ignoreStatus);
    }

    /**
     * Getter for the email sender
     *
     * @return \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender
     */
    public function getEmailSender()
    {
        return $this->traitGetEmailSender();
    }

    /**
     * Register quotation for so data can be collected in template
     *
     * @param \Magento\Framework\DataObject|\Magento\Sales\Model\AbstractModel $quote
     * @return void
     */
    public function registerAsyncQuotation($quote)
    {
        $this->traitRegisterAsyncQuotation($quote);
    }
}
